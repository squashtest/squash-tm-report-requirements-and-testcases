/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.dto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public class Data {

	private Collection<ProjectData> projects;
	private static final String LABEL_REQUIREMENTS_BOUNDREQUIREMENT = "report.books.requirements.label.requirement.boundrequirement";
	private static final String lABEL_REQUIREMENT_ANCESTORS = "report.books.requirements.label.requirement.ancestors";
	private static final String LABEL_REQUIREMENT_CHILDREN = "report.books.requirements.label.requirement.children";
	private static final String ATTACHED_TESTS_TITLE = "report.books.requirements.attached.tests.title";
	private static final String BOOK_CUF_DATE_FORMAT = "report.books.requirements.book.cuf.dateFormat";
	private static final String BOOK_REQUIREMENT_ATTACHMENTS = "report.books.requirements.book.requirement.attachments";
	private static final String BOOK_REQUIREMENT_CATEGORY = "report.books.requirements.requirement.category";
	private static final String BOOK_REQUIREMENT_CREATED = "report.books.requirements.book.requirement.createdOn";
	private static final String BOOK_REQUIREMENT_CREATED_BY = "report.books.requirements.book.requirement.created.by";
	private static final String BOOK_REQUIREMENT_CREATED_ON = "report.books.requirements.book.requirement.created.on";
	private static final String BOOK_REQUIREMENT_CRITICALITY = "report.books.requirements.criticality";
	private static final String BOOK_REQUIREMENT_DATE_FORMAT = "report.books.requirements.book.requirement.dateformat";
	private static final String BOOK_REQUIREMENT_FOOTER = "report.books.requirements.book.requirement.footer";
	private static final String BOOK_REQUIREMENT_GENERATED_BY = "report.books.requirements.book.requirement.generatedby";
	private static final String BOOK_REQUIREMENT_ID = "report.books.requirements.book.requirement.id";
	private static final String BOOK_REQUIREMENT_MODIFIED = "report.books.requirements.book.requirement.modifiedOn";
	private static final String BOOK_REQUIREMENT_MODIFIED_BY = "report.books.requirements.book.requirement.modified.by";
	private static final String BOOK_REQUIREMENT_MODIFIED_ON = "report.books.requirements.book.requirement.modified.on";
	private static final String LABEL_CUF = "report.books.requirements.book.requirement.cuf";
	private static final String BOOK_REQUIREMENT_STATUS = "report.books.requirements.status";
	private static final String BOOK_REQUIREMENT_TITLE = "report.books.requirements.book.requirement.title";
	private static final String BOOK_REQUIREMENT_VERSION = "report.books.requirements.book.requirement.version";
	private static final String BOOK_REQUIREMENT_VERSION_ID = "report.books.requirements.book.requirement.version-id";
	private static final String BOOK_TESTCASE_IMPORTANCE = "report.books.requirements.testcase.importance";
	private static final String BOOK_TESTCASE_NAME = "report.books.requirements.testcase.name";
	private static final String BOOK_TESTCASE_PROJECT_NAME = "report.books.requirements.testcase.projectname";
	private static final String BOOK_TESTCASE_TITLE = "report.books.requirements.testcase.title";
	private static final String BOOK_TOC = "report.books.requirements.book.toc";
	private static final String CRITICITY = "report.books.requirements.criticity";
	private static final String IMPORTANCE = "report.books.requirements.importance";
	private static final String LABEL_PAGE = "report.books.requirements.label.page";
	private static final String NAME = "report.books.requirements.name";
	private static final String REQUIREMENT = "report.books.requirements.requirement";
	private static final String REQUIREMENT_DESCRIPTION = "report.books.requirements.requirement.description";
	private static final String REQUIREMENT_NAME = "report.books.requirements.requirement.name";
	private static final String REQUIREMENT_REFERENCE = "report.books.requirements.requirement.reference";
	private static final String REQUIREMENTS_CREATED = "report.books.requirements.requirements.created";
	private static final String REQUIREMENTS_MODIFIED = "report.books.requirements.requirements.modified";
	private static final String REQUIREMENTS_LIST_REPORT_LABEL = "report.books.requirements.requirementsList.report.label";
	private static final String SUMMARY = "report.books.requirements.summary";
	private static final String VIEW_TITLE_REQUIREMENTS_REPORT = "report.books.requirements.view.title.requirements.report";
	private static final String VIEW_TITLE_DATEFORMAT = "report.books.requirements.title.dateformat";
	private static final String VIEW_TITLE_REQUIREMENTS_LIST_REPORT = "report.books.requirements.view.title.requirementsList.report";



	private static final String GENERATED_TITLE = "report.books.requirements.generated.title";
	private static final String GENERATED_DATE = "report.books.requirements.book.dateformat";
	private static final String GENERATED_HOUR = "report.books.requirements.book.hourformat";

	private static final String MILESTONE_TITLE = "report.books.requirements.requirement.title.milestone";
	private static final String LABEL_MILESTONES = "report.books.requirements.requirement.milestones";

	private static final String INDEX = "report.books.requirements.index";

	private static final String REQUIREMENT_LINK_TITLE = "requirement-version.linked-requirement-version.panel.title";
	private static final String REQUIREMENT_LINK_PROJECT_LABEL = "requirement-version.linked-requirement-version.project.title";
	private static final String REQUIREMENT_LINK_REQUIREMENT_LABEL = "requirement-version.linked-requirement-version.requirement.title";
	private static final String REQUIREMENT_LINK_REQUIREMENT_VERSION = "requirement-version.linked-requirement-version.requirement.version";
	private static final String REQUIREMENT_LINK_REQUIREMENT_ROLE = "requirement-version.linked-requirement-version.requirement.role";

	private final String milestoneName;

	public Data(Collection<ProjectData> projectDataList, String milestoneLabel) {
		super();
		this.projects = projectDataList;
		this.milestoneName = milestoneLabel;
	}

	public Collection<ProjectData> getProjects() {
		return projects;
	}

	private String translate(String string) {
		return I18nHelper.translate(string);
	}

	private String translate(String string, Object[] params) {
		return I18nHelper.translate(string, params);
	}

	public String getAttachedTestsTitle() {
		return translate(ATTACHED_TESTS_TITLE);
	}

	public String getBookCufDateFormat() {
		return translate(BOOK_CUF_DATE_FORMAT);
	}

	public String getLabelAttachment() {
		return translate(BOOK_REQUIREMENT_ATTACHMENTS);
	}

	public String getLabelCUF() {
		return translate(LABEL_CUF);
	}

	public String getLabelCategory() {
		return translate(BOOK_REQUIREMENT_CATEGORY);
	}

	public String getLabelCreated() {
		return translate(BOOK_REQUIREMENT_CREATED);
	}

	public String getLabelCreatedBy() {
		return translate(BOOK_REQUIREMENT_CREATED_BY);
	}

	public String getLabelCreatedOn() {
		return translate(BOOK_REQUIREMENT_CREATED_ON);
	}

	public String getBookRequirementCriticality() {
		return translate(BOOK_REQUIREMENT_CRITICALITY);
	}

	public String getBookRequirementDateformat() {
		return translate(BOOK_REQUIREMENT_DATE_FORMAT);
	}

	public String getBookRequirementFooter() {
		return translate(BOOK_REQUIREMENT_FOOTER);
	}

	public String getBookRequirementGeneratedby() {
		return translate(BOOK_REQUIREMENT_GENERATED_BY);
	}

	public String getLabelId() {
		return translate(BOOK_REQUIREMENT_ID);
	}

	public String getLabelModified() {
		return translate(BOOK_REQUIREMENT_MODIFIED);
	}

	public String getLabelModifiedBy() {
		return translate(BOOK_REQUIREMENT_MODIFIED_BY);
	}

	public String getLabelModifiedOn() {
		return translate(BOOK_REQUIREMENT_MODIFIED_ON);
	}

	public String getLabelStatus() {
		return translate(BOOK_REQUIREMENT_STATUS);
	}

	public String getBookRequirementTitle() {
		return translate(BOOK_REQUIREMENT_TITLE);
	}

	public String getLabelVersion() {
		return translate(BOOK_REQUIREMENT_VERSION);
	}

	public String getLabelVersionId() {
		return translate(BOOK_REQUIREMENT_VERSION_ID);
	}

	public String getBookTestcaseImportance() {
		return translate(BOOK_TESTCASE_IMPORTANCE);
	}

	public String getBookTestcaseName() {
		return translate(BOOK_TESTCASE_NAME);
	}

	public String getBookTestcaseProjectname() {
		return translate(BOOK_TESTCASE_PROJECT_NAME);
	}

	public String getBookTestcaseTitle() {
		return translate(BOOK_TESTCASE_TITLE);
	}

	public String getBookToc() {
		return translate(BOOK_TOC);
	}

	public String getLabelCriticity() {
		return translate(CRITICITY);
	}

	public String getImportance() {
		return translate(IMPORTANCE);
	}

	public String getLabelPage() {
		return translate(LABEL_PAGE);
	}

	public String getName() {
		return translate(NAME);
	}

	public String getRequirement() {
		return translate(REQUIREMENT);
	}

	public String getLabelDescription() {
		return translate(REQUIREMENT_DESCRIPTION);
	}

	public String getRequirementName() {
		return translate(REQUIREMENT_NAME);
	}

	public String getRequirementReference() {
		return translate(REQUIREMENT_REFERENCE);
	}

	public String getRequirementsCreated() {
		return translate(REQUIREMENTS_CREATED);
	}

	public String getRequirementsModified() {
		return translate(REQUIREMENTS_MODIFIED);
	}

	public String getRequirementsListReportLabel() {
		return translate(REQUIREMENTS_LIST_REPORT_LABEL);
	}

	public String getSummary() {
		return translate(SUMMARY);
	}

	// issue 4712
	public String getViewTitleRequirementsReport() {
		String title = translate(VIEW_TITLE_REQUIREMENTS_REPORT);
		SimpleDateFormat formatter = new SimpleDateFormat(translate(VIEW_TITLE_DATEFORMAT));
		String strDate = formatter.format(new Date());
		return title + "-" + strDate;
	}

	public String getViewTitleRequirementsListReport() {
		return translate(VIEW_TITLE_REQUIREMENTS_LIST_REPORT);
	}

	public String getLabelBoundRequirements() {
		return translate(LABEL_REQUIREMENTS_BOUNDREQUIREMENT);
	}

	public String getLabelChildren() {
		return translate(LABEL_REQUIREMENT_CHILDREN);
	}

	public String getLabelAncestors() {
		return translate(lABEL_REQUIREMENT_ANCESTORS);
	}

	public String getRequirementLinkTitle() {
		return translate(REQUIREMENT_LINK_TITLE);
	}

	public String getRequirementLinkProjectLabel() {
		return translate(REQUIREMENT_LINK_PROJECT_LABEL);
	}
	public String getRequirementLinkRequirementLabel() {
		return translate(REQUIREMENT_LINK_REQUIREMENT_LABEL);
	}
	public String getRequirementLinkRequirementVersion() {
		return translate(REQUIREMENT_LINK_REQUIREMENT_VERSION);
	}

	public String getRequirementLinkRequirementRole() {
		return translate(REQUIREMENT_LINK_REQUIREMENT_ROLE);
	}

	public String getLabelMilestones() {
		return translate(LABEL_MILESTONES);
	}

	public String getTitleMilestone() {
		return (milestoneName != null) ? translate(MILESTONE_TITLE) + " " + milestoneName : "";
	}

	public String getGeneratedDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(translate(GENERATED_DATE));
		Calendar cal = Calendar.getInstance();
		return sdfDate.format(cal.getTime());
	}

	public String getGeneratedHour() {
		SimpleDateFormat sdfHour = new SimpleDateFormat(translate(GENERATED_HOUR));
		Calendar cal = Calendar.getInstance();
		return sdfHour.format(cal.getTime());
	}

	public String getGeneratedTitle() {

		Object[] params = new Object[2];
		params[0] = getGeneratedDate();
		params[1] = getGeneratedHour();
		return translate(GENERATED_TITLE, params);
	}

	public String getIndex(){
		return translate(INDEX);
	}

}
