/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.dto;

import org.apache.commons.lang.StringUtils;

import java.util.*;

public class RequirementVersionData {

	private Long parentFolderId;
	private Long parentReqVersionId;
	private Long projectId;
	private Long versionId;
	private Long requirementId;
	private String reference;
	private int versionNumber;
	private String criticality;
	private String status;
	private String category;
	private String categoryType;
	private int nbAttachments;
	private String name;
	private String milestoneNames;
	private String path = null;
	private String pathDescription; // 6928 - Add folder description
	private String description;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	private Date modifiedOn;
	private int totalVersionNumber;

	private List<TestCaseData> boundTestCases = new LinkedList<TestCaseData>();
	private Map<String, TestCaseProject> testCaseProjects = new LinkedHashMap<String, TestCaseProject>();

	private List<RequirementVersionDataAdapter> ancestors = new LinkedList<RequirementVersionDataAdapter>();
	private List<RequirementVersionData> childrenRequirements = new LinkedList<RequirementVersionData>();

	private RequirementVersionDataAdapter parentRequirementVersion;

	private FolderData parentFolder;

	private List<CufData> cufs = new ArrayList<CufData>();
	private List<CufData> rtfCufs = new ArrayList<CufData>();
	private List<CufData> numCufs = new ArrayList<CufData>();
	private List<CufData> tagCufs = new ArrayList<CufData>();
	private List<ReqLinkData> reqLinks = new ArrayList<ReqLinkData>();

	// ****** meta properties for rendering usage ******

	private boolean shouldPrintPath = false;
	private boolean hasReqLink = true;

	// ****** methods *********

	public List<CufData> getTagCufs() {
		return tagCufs;
	}

	public void setTagCufs(List<CufData> tagCufs) {
		this.tagCufs = tagCufs;
	}

	public Long getParentFolderId() {
		return parentFolderId;
	}

	public void setParentFolderId(Long parentFolderId) {
		this.parentFolderId = parentFolderId;
	}

	public Long getParentReqVersionId() {
		return parentReqVersionId;
	}

	FolderData getParentFolder() {
		return parentFolder;
	}

	public void setParentFolder(FolderData parentFolder) {
		this.parentFolder = parentFolder;
	}

	public List<RequirementVersionData> getChildrenRequirements() {
		return childrenRequirements;
	}

	public void setChildrenRequirements(List<RequirementVersionData> childrenRequirements) {
		this.childrenRequirements = childrenRequirements;
	}

	public boolean isShouldPrintPath() {
		return shouldPrintPath;
	}

	public void setShouldPrintPath(boolean shouldPrintPath) {
		this.shouldPrintPath = shouldPrintPath;
	}

	public RequirementVersionDataAdapter getParentRequirementVersion() {
		return parentRequirementVersion;
	}

	public void setTestCaseProjects(Map<String, TestCaseProject> testCaseProjects) {
		this.testCaseProjects = testCaseProjects;
	}

	public void setAncestors(List<RequirementVersionDataAdapter> ancestors) {
		this.ancestors = ancestors;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setParentReqVersionId(Long parentReqVersionId) {
		this.parentReqVersionId = parentReqVersionId;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getPathDescription() { return pathDescription; }

	public void setPathDescription(String pathDescription) { this.pathDescription = pathDescription; }

	public String getPath() {

		// init if not ready
		if (path == null) {
			if (hasParentRequirementVersion()) {

				String parentPath = parentRequirementVersion.getPath();
				String parentName = parentRequirementVersion.getName();
				if (StringUtils.isBlank(parentPath)) {
					path = parentName;
				} else {
					path = parentPath + FolderData.BREADCRUMB_SEPARATOR + parentName;
				}

			} else {
				path = getParentFolder().getBreadcrumb();
			}
		}
		return path;
	}

	private String translate(String string) {
		return I18nHelper.translate(string);
	}

	public boolean hasParentRequirementVersion() {
		return (this.parentReqVersionId != null);
	}

	public List<RequirementVersionDataAdapter> getAncestors() {
		return ancestors;
	}

	public void setParentRequirementVersion(RequirementVersionDataAdapter parentRequirementVersion) {
		this.parentRequirementVersion = parentRequirementVersion;
	}

	public void addChildrenRequirement(RequirementVersionData requirement) {
		this.childrenRequirements.add(requirement);
		RequirementVersionDataAdapter parent = new RequirementVersionDataAdapter(this);
		requirement.setParentRequirementVersion(parent);
		requirement.addAncestor(parent);
		requirement.addAncestors(this.ancestors);
	}

	public void addAncestor(RequirementVersionDataAdapter ancestor) {
		this.ancestors.add(ancestor);
	}

	public void addAncestors(List<RequirementVersionDataAdapter> ancestors) {
		this.ancestors.addAll(ancestors);
	}

	public String getParentRequirementDisplayableName() {
		if (hasParentRequirementVersion()) {
			RequirementVersionDataAdapter parent = this.parentRequirementVersion;
			return parent.getName() + " (ID " + parent.getRequirementId().toString() + ")";
		} else {
			return "";
		}
	}

	public List<RequirementVersionData> getChildren() {
		return this.childrenRequirements;
	}

	public List<RequirementVersionData> getAllChildren() {
		List<RequirementVersionData> allChildren = new LinkedList<RequirementVersionData>(this.childrenRequirements);
		for (RequirementVersionData child : this.childrenRequirements) {
			allChildren.addAll(child.getAllChildren());
		}
		return allChildren;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Integer getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getCriticality() {

		String string = "";
		switch (criticality) {
		case "MAJOR":
			string = "report.books.requirements.criticality.major";
			break;
		case "MINOR":
			string = "report.books.requirements.criticality.minor";
			break;
		case "CRITICAL":
			string = "report.books.requirements.criticality.critical";
			break;
		default:
			string = "report.books.requirements.criticality.undefined";
		}

		return translate(string);
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getStatus() {
		String string = "";
		switch (status) {
		case "APPROVED":
			string = "report.books.requirements.status.approved";
			break;
		case "OBSOLETE":
			string = "report.books.requirements.status.obsolete";
			break;
		case "UNDER_REVIEW":
			string = "report.books.requirements.status.under_review";
			break;
		case "WORK_IN_PROGRESS":
			string = "report.books.requirements.status.work_in_progress";
		}

		return translate(string);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCategory() {

		if (categoryType.equals("SYS")) {
			return I18nHelper.translate("report.books.requirements." + category.toLowerCase());
		} else {
			return category;
		}
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getNbAttachments() {
		return nbAttachments;
	}

	public void setNbAttachments(int nbAttachments) {
		this.nbAttachments = nbAttachments;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {

		return description != null ? description : "";
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		if (createdOn != null) {
			return I18nHelper.translate(createdOn);
		} else {
			return "";
		}
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public List<ReqLinkData> getReqLinks() {
		return reqLinks;
	}

	public void setReqLinks(List<ReqLinkData> reqLinks) {
		this.reqLinks = reqLinks;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedOn() {
		if (modifiedOn != null) {
			return I18nHelper.translate(modifiedOn);
		} else {
			return "";
		}
	}

	public void setModifiedOn(Date modifuedOn) {
		this.modifiedOn = modifuedOn;
	}

	public Collection<TestCaseData> getBoundTestCases() {
		return boundTestCases;
	}

	public void setBoundTestCases(List<TestCaseData> boundTestCases) {
		this.boundTestCases = boundTestCases;
	}

	public List<CufData> getCufs() {
		return cufs;
	}

	public void setCufs(List<CufData> cufs) {
		this.cufs = cufs;
	}

	public void setRtfCufs(List<CufData> cufs) {
		this.rtfCufs = cufs;
	}

	public List<CufData> getRtfCufs() {
		return rtfCufs;
	}

	public List<CufData> getNumCufs() {
		return numCufs;
	}

	public void setNumCufs(List<CufData> numCufs) {
		this.numCufs = numCufs;
	}

	public void addAllTestCases(Collection<TestCaseData> data) {
		boundTestCases.addAll(data);
	}

	public void addTestCase(TestCaseData data) {
		boundTestCases.add(data);
		String projectName = data.getProjectName();
		TestCaseProject project = testCaseProjects.get(projectName);

		if (project != null) {
			project.addTestCase(data);
		} else {
			project = new TestCaseProject();
			project.setProjectName(projectName);
			project.addTestCase(data);
			testCaseProjects.put(projectName, project);
		}
	}

	public String getMilestoneNames() {
		return milestoneNames;
	}

	public void setMilestoneNames(String milestoneLabels) {
		this.milestoneNames = milestoneLabels;
	}

	public int getTotalVersionNumber() {
		return totalVersionNumber;
	}

	public void setTotalVersionNumber(int totalVersionNumber) {
		this.totalVersionNumber = totalVersionNumber;
	}

	public boolean hasAttachments() {
		return (nbAttachments > 0);
	}

	public boolean hasParentFolder() {
		return parentFolderId != null;
	}

	public boolean acceptsAsContent(TestCaseData testCase) {
		return testCase.getRequirementVersionId().equals(versionId);
	}

	public boolean acceptsAsCuf(CufData cufData) {
		return cufData.getReqVersionId().equals(versionId);
	}

	public boolean acceptsAsReqLink(ReqLinkData cufData) {
		return cufData.getReqVersionId().equals(versionId);
	}

	public List<TestCaseProject> getTestCaseProjects() {
		return new LinkedList<TestCaseProject>(testCaseProjects.values());
	}

	public void sortContent() {
		Collections.sort(boundTestCases, new TestCaseSorter());
	}

	private class TestCaseSorter implements Comparator<TestCaseData> {

		public int compare(TestCaseData tc1, TestCaseData tc2) {
			String name1 = normalizeName(tc1);
			String name2 = normalizeName(tc2);

			return name1.compareTo(name2);
		}

		private String normalizeName(TestCaseData data) {
			if (StringUtils.isNotBlank(data.getReference())) {
				return data.getReference() + " - " + data.getName();
			} else {
				return data.getName();
			}
		}

	}

	// ****** meta informations for the renderer **********

	public boolean isNoPrintPath() {
		boolean emptyPath = StringUtils.isBlank(getPath());

		return emptyPath || (!shouldPrintPath);
	}

	public boolean isHasReqLink() {
		return hasReqLink;
	}

	public void setHasReqLink(boolean hasReqLink) {
		this.hasReqLink = hasReqLink;
	}

	public boolean isNoReference() {
		return StringUtils.isBlank(reference);
	}

	public boolean isUnmodified() {
		return modifiedOn == null;
	}

	public boolean isNoCustomField() {
		return cufs.size() == 0 && rtfCufs.size() == 0 && numCufs.size() == 0 && tagCufs.size() == 0;
	}

	public boolean isNoBoundTestCase() {
		return boundTestCases.size() == 0;
	}

	public boolean isNoBoundRequirement() {
		return childrenRequirements.size() == 0 && ancestors.size() == 0;
	}

}
