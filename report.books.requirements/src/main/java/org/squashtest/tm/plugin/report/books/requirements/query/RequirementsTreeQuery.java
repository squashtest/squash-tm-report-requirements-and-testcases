/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.requirements.query;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;
import org.squashtest.tm.plugin.report.books.requirements.dto.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class RequirementsTreeQuery implements ReportQuery, InitializingBean {

	private static Map<Resource, String> cache = new HashMap<Resource, String>();
	private static final String MILESTONES = "milestones";
	private static final String REQUIREMENTS_IDS = "requirementsIds";
	private static final String PROJECT_IDS = "projectIds";
	private static final String REQUIREMENTS_SELECTION_MODE = "requirementsSelectionMode";

	private static final String REQUIREMENT_FOLDERS = "requirement-folders";
	private static final String REQUIREMENTS = "requirements";
	private static final Object TAGS = "tags";

	private RequirementTreeQueryFinder queryFinder = new RequirementTreeQueryFinder();
	private RequirementTreeQueryFormatter formatter = new RequirementTreeQueryFormatter();

	protected SqlQueryRunner runner;

	public void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	public void afterPropertiesSet() throws Exception {
		queryFinder.setRunner(runner);
		formatter.setQueryFinder(queryFinder);
	}

	public void executeQuery(Map<String, Criteria> crit, Map<String, Object> res) {

		// get the target requirement ids
		Collection<Long> versionIds = getReqVersionIdList(crit);

		// get the model
		Collection<Object[]> rawFolderData = queryFinder.getFoldersDataForVersionIds(versionIds);
		Collection<Object[]> rawProjectData = queryFinder.getProjectDataForVersionIds(versionIds);
		Collection<Object[]> rawReqVersionData = queryFinder.getReqVersionsDataForVersionIds(versionIds);
		Collection<Object[]> rawBoundTCData = queryFinder.getTestCasesDataForVersionIds(versionIds);
		Collection<Object[]> rawCufData = queryFinder.getCufsDataForVersionIds(versionIds);
		Collection<Object[]> rawRtfCufData = queryFinder.getRtfCufsDataForVersionIds(versionIds);
		Collection<Object[]> rawNumCufData = queryFinder.getNumCufsDataForVersionIds(versionIds);
		Collection<Object[]> rawtagCufData = queryFinder.getTagCufsDataForVersionIds(versionIds);
		Collection<Object[]> rawreqLinkData = queryFinder.getreqLinkDataQueryForReqIds(versionIds);

		// make dto of them
		Collection<ProjectData> projectDataList = formatter.toProjectData(rawProjectData);
		Collection<FolderData> folderDataList = formatter.toFolderData(projectDataList, rawFolderData);
		Collection<TestCaseData> testCaseDataList = formatter.toTestCaseData(rawBoundTCData);
		Collection<CufData> cufDataList = formatter.toCufData(rawCufData);
		Collection<CufData> rtfCufDataList = formatter.toCufData(rawRtfCufData);
		Collection<CufData> numCufDataList = formatter.toCufData(rawNumCufData);
		Collection<CufData> tagCufDataList = formatter.toMultipleValueCufData(rawtagCufData);
		Collection<RequirementVersionData> versionDataList = formatter.toRequirementVersionData(rawReqVersionData);
		Collection<ReqLinkData> reqLinkDataList = formatter.toReqLinkData(rawreqLinkData);
		// bind them
		formatter.bindAllData(projectDataList, folderDataList, versionDataList, testCaseDataList, cufDataList,
				rtfCufDataList, numCufDataList, tagCufDataList,reqLinkDataList);

		// special milestone mode
		String milestoneLabel = null;
		if (isMilestonePicker(crit)) {
			List<Integer> milestoneIds = (List<Integer>) crit.get(MILESTONES).getValue();
			milestoneLabel = queryFinder.getMilestoneLabel(milestoneIds.get(0));
			res.put("milestoneLabel", milestoneLabel);
		}

		List<String> html = new ArrayList<String>();
		processRichText(projectDataList, folderDataList, html);
		Data data = new Data(projectDataList, milestoneLabel);
		res.put("data", data);
		res.put("fileName", data.getViewTitleRequirementsReport());
		res.put("data", data);
		res.put("html", html);
	}

	private void processRichText(Collection<ProjectData> projectDataList, Collection<FolderData> folderDataList, List<String> html) {

		for (ProjectData project : projectDataList) {
			for (RequirementVersionData req : project.getRequirements()) {
				if (req.hasParentFolder()) {
					folderDescriptionForReq(req, folderDataList, html);
				}
				req.setDescription(richTextReplace(req.getDescription(), html));
				richTextForCuf(req.getRtfCufs(), html);
			}
		}
	}

	private void folderDescriptionForReq(RequirementVersionData req, Collection<FolderData> folderDataList, List<String> html) {
		for (FolderData folder : folderDataList) {
			if (folder.getDescription() != null && folder.getFolderId().equals(req.getParentFolderId())) {
				req.setPathDescription(richTextReplace(folder.getDescription(), html));
			}
		}
	}

	private void richTextForCuf(List<CufData> cufs, List<String> html) {
		for (CufData cuf : cufs) {
			cuf.setValue(richTextReplace(cuf.getValue(), html));
		}
	}
	private String richTextReplace(String string, List<String> html) {

		StringBuilder sb = new StringBuilder(string);
		sb.insert(0, "<html>");
		sb.append("</html>");
		String newVal = "<w:altChunk r:id='toto" + html.size() + "'/>";
		html.add(sb.toString());

		return newVal;
	}

	// ******************** query finder configuration ********************

	public void setIdsByProjectQuery(Resource idsByProjectQuery) {
		String query = loadQuery(idsByProjectQuery);
		queryFinder.setIdsByProjectQuery(query);
	}

	public void setLastVersionIdsByProjectQuery(Resource lastVersionIdsByProjectQuery) {
		String query = loadQuery(lastVersionIdsByProjectQuery);
		queryFinder.setLastVersionIdsByProjectQuery(query);
	}

	public void setIdsByTagQuery(Resource idsByTagQuery) {
		String query = loadQuery(idsByTagQuery);
		queryFinder.setIdsByTagQuery(query);
	}

	public void setLastVersionIdsByTagQuery(Resource lastVersionIdsByTagQuery) {
		String query = loadQuery(lastVersionIdsByTagQuery);
		queryFinder.setLastVersionIdsByTagQuery(query);
	}

	public void setIdsForAllProjectsQuery(Resource idsForAllProjectsQuery) {
		String query = loadQuery(idsForAllProjectsQuery);
		queryFinder.setIdsForAllProjectsQuery(query);
	}

	public void setLastVersionIdsForAllProjectsQuery(Resource lastVersionIdsForAllProjectsQuery) {
		String query = loadQuery(lastVersionIdsForAllProjectsQuery);
		queryFinder.setLastVersionIdsForAllProjectsQuery(query);
	}

	public void setIdsBySelectionQuery(Resource idsBySelectionQuery) {
		String query = loadQuery(idsBySelectionQuery);
		queryFinder.setIdsBySelectionQuery(query);
	}

	public void setLastVersionIdsBySelectionQuery(Resource lastVersionIdsBySelectionQuery) {
		String query = loadQuery(lastVersionIdsBySelectionQuery);
		queryFinder.setLastVersionIdsBySelectionQuery(query);
	}

	public void setIdsByMilestoneQuery(Resource idsByMilestoneQuery) {
		String query = loadQuery(idsByMilestoneQuery);
		queryFinder.setIdsByMilestoneQuery(query);
	}

	public void setLastVersionIdsByMilestoneQuery(Resource lastVersionIdsByMilestoneQuery) {
		String query = loadQuery(lastVersionIdsByMilestoneQuery);
		queryFinder.setLastVersionIdsByMilestoneQuery(query);
	}

	public void setRequirementVersionDataQuery(Resource requirementVersionDataQuery) {
		String query = loadQuery(requirementVersionDataQuery);
		queryFinder.setRequirementVersionDataQuery(query);
	}

	public void setFolderDataQuery(Resource folderDataQuery) {
		String query = loadQuery(folderDataQuery);
		queryFinder.setFolderDataQuery(query);
	}

	public void setProjectDataQuery(Resource projectDataQuery) {
		String query = loadQuery(projectDataQuery);
		queryFinder.setProjectDataQuery(query);
	}

	public void setBoundTCDataQuery(Resource boundTCDataQuery) {
		String query = loadQuery(boundTCDataQuery);
		queryFinder.setBoundTCDataQuery(query);
	}

	public void setCufsDataQuery(Resource cufsDataQuery) {
		String query = loadQuery(cufsDataQuery);
		queryFinder.setCufsDataQuery(query);
	}

	public void setRtfCufsDataQuery(Resource rtfCufsDataQuery) {
		String query = loadQuery(rtfCufsDataQuery);
		queryFinder.setRtfCufsDataQuery(query);
	}

	public void setNumCufsDataQuery(Resource numCufsDataQuery) {
		String query = loadQuery(numCufsDataQuery);
		queryFinder.setNumCufsDataQuery(query);
	}

	public void setTagCufsDataQuery(Resource tagCufsDataQuery) {
		String query = loadQuery(tagCufsDataQuery);
		queryFinder.setTagCufsDataQuery(query);
	}

	public void setReqLinkDataQuery(Resource reqLinkDataQuery) {
		String query = loadQuery(reqLinkDataQuery);
		queryFinder.setReqLinkDataQuery(query);
	}


	protected SqlQueryRunner getRunner() {
		return runner;
	}

	// ********************* private stuffs ***************************

	@SuppressWarnings("unchecked")
	private Collection<Long> getReqVersionIdList(Map<String, Criteria> criteriaMap) {

		Criteria selectionMode = criteriaMap.get(REQUIREMENTS_SELECTION_MODE);
		Map<String, Boolean> optionsMap = new HashMap<String, Boolean>();

		Criteria options = criteriaMap.get("reportOptions");
		optionsMap.put("printOnlyLastVersion", false);

		Collection<String> selectedOptions = (Collection<String>) options.getValue();

		for (String option : selectedOptions) {
			optionsMap.put(option, true);
		}
		Collection<Long> requirementIdList;
		Boolean printOnlyLastVersion = optionsMap.get("printOnlyLastVersion");

		if ("PROJECT_PICKER".equals(selectionMode.getValue())) {
			Criteria criteria = criteriaMap.get(PROJECT_IDS);
			Collection<String> projectIds = (Collection<String>) criteria.getValue();
			requirementIdList = queryFinder.findIdsByProject(projectIds, printOnlyLastVersion);
		}

		else if ("MILESTONE_PICKER".equals(selectionMode.getValue())) {
			List<String> milestoneIds = (List<String>) criteriaMap.get(MILESTONES).getValue();
			requirementIdList = queryFinder.findIdsByMilestone(milestoneIds, printOnlyLastVersion);
		}

		else if ("TAG_PICKER".equals(selectionMode.getValue())){
			List<String> tags = (List<String>) criteriaMap.get(TAGS).getValue();
			requirementIdList = queryFinder.findIdsByTags(tags, printOnlyLastVersion);

		}

		else {
			Criteria criteria = criteriaMap.get(REQUIREMENTS_IDS);
			Map<String, Collection<?>> selectedIds = (Map<String, Collection<?>>) (criteria.getValue());

			Collection<String> allStrIds = (Collection<String>) selectedIds.get(REQUIREMENTS);
			if (allStrIds == null) {
				allStrIds = new ArrayList<String>();
			}
			Collection<String> folderIds = (Collection<String>) selectedIds.get(REQUIREMENT_FOLDERS);
			if (folderIds == null) {
				folderIds = new ArrayList<String>();
			}

			allStrIds.addAll(folderIds);

			requirementIdList = queryFinder.findIdsBySelection(allStrIds, printOnlyLastVersion);
		}

		return requirementIdList;
	}

	private boolean isMilestonePicker(Map<String, Criteria> criteriaMap) {
		Criteria selectionMode = criteriaMap.get(REQUIREMENTS_SELECTION_MODE);

		return "MILESTONE_PICKER".equals(selectionMode.getValue());
	}

	protected static String loadQuery(Resource query) {
		if (cache.get(query) == null) {
			InputStream is;
			try {
				is = query.getInputStream();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			Scanner scan = new Scanner(is, "UTF-8");
			String value = scan.useDelimiter("\\A").next();
			scan.close();
			cache.put(query, value);
		}
		return cache.get(query);
	}
}
