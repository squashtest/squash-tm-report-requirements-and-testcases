select distinct TC.TCLN_ID
from TEST_CASE TC
inner join TCLN_RELATIONSHIP_CLOSURE TCLNC on TCLNC.DESCENDANT_ID = TC.TCLN_ID
where TCLNC.ANCESTOR_ID in (:nodeIds)
