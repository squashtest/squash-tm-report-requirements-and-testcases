package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.util.ArrayList;
import java.util.List;

public class Dataset {

	private Long id;
	private String name;
	private Long testCaseId;
	private List<DatasetParamValue> datasetParamValues = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public List<DatasetParamValue> getDatasetParamValues() {
		return datasetParamValues;
	}

	public void setDatasetParamValues(List<DatasetParamValue> datasetParamValues) {
		this.datasetParamValues = datasetParamValues;
	}
}
