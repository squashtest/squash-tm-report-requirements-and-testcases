/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Data {

	public static final String KEY_PREFIX = "report.books.testcases.";

	private List<Project> projects;

	private static final String labelDescription  = KEY_PREFIX + "label.Description";
	private static final String labelAttachment  = KEY_PREFIX + "label.attachment";
	private static final String labelAutomated  = KEY_PREFIX + "label.automated";
	private static final String labelCreated  = KEY_PREFIX + "label.createdOn";
	private static final String labelId  = KEY_PREFIX + "label.id";
	private static final String labelImportance  = KEY_PREFIX + "label.importance";
	private static final String labelLastModified  = KEY_PREFIX + "label.lastModifiedOn";
	private static final String labelNature  = KEY_PREFIX + "label.nature";
	private static final String labelPage  = KEY_PREFIX + "label.page";
	private static final String labelProjet  = KEY_PREFIX + "label.projet";
	private static final String labelStatus  = KEY_PREFIX + "label.status";
	private static final String labelTestcasesreport  = KEY_PREFIX + "label.testcasesreport";
	private static final String labelType  = KEY_PREFIX + "label.type";
	private static final String labelVersion  = KEY_PREFIX + "label.version";
	private static final String linkedRequirementsName  = KEY_PREFIX + "linked.requirements.name";
	private static final String linkedRequirementsProjectName  = KEY_PREFIX + "linked.requirements.project.name";
	private static final String linkedRequirementsTitle  = KEY_PREFIX + "linked.requirements.title";
	private static final String summary  = KEY_PREFIX + "summary";
	private static final String testcaseCreatedBy  = KEY_PREFIX + "createdBy";
	private static final String testcaseCreatedOn  = KEY_PREFIX + "createdOn";
	private static final String testcaseExecutionMode  = KEY_PREFIX + "executionMode";
	private static final String testcaseImportance  = KEY_PREFIX + "importance";
	private static final String testcaseModifiedBy  = KEY_PREFIX + "modifiedBy";
	private static final String testcaseModifiedOn  = KEY_PREFIX + "modifiedOn";
	private static final String testcaseStepsAction  = KEY_PREFIX + "steps.action";
	private static final String testcaseStepsCall  = KEY_PREFIX + "steps.call";
	private static final String testcaseStepsCallDataset  = KEY_PREFIX + "steps.call.dataset";
	private static final String testcaseStepsExpectedResult  = KEY_PREFIX + "steps.expectedResult";
	private static final String testcaseStepsTitle  = KEY_PREFIX + "steps.title";
	private static final String testcasesReportLabel  = KEY_PREFIX + "report.label";
	private static final String testcasesTitle  = KEY_PREFIX + "title";
	private static final String testcasesIndex  = KEY_PREFIX + "index";
	private static final String testcasesTitleDateformat  = KEY_PREFIX + "title.dateformat";
	private static final String generated  = KEY_PREFIX + "generated";
	private static final String prerequisite  = KEY_PREFIX + "prerequisite";
	private static final String generatedTitle  = KEY_PREFIX + "generated.title";
	private static final String generatedDate  = KEY_PREFIX + "book.dateformat";
	private static final String generatedHour  = KEY_PREFIX + "book.hourformat";
	private static final String labelMilestones  = KEY_PREFIX + "milestones";
	private static final String titleMilestone  = KEY_PREFIX + "title.milestone";
	private static final String scriptTitle = KEY_PREFIX + "script.title";
	private static final String parameterTitle = KEY_PREFIX + "parameter.title";
	private static final String tableName = KEY_PREFIX + "table.name";
	private static final String tableDescription = KEY_PREFIX + "table.description";
	private static final String tableTestCaseSource = KEY_PREFIX + "table.testcase.source";
	private static final String tableValue = KEY_PREFIX + "table.value";
	private static final String datasetTitle = KEY_PREFIX + "dataset.title";
	private static final String labelAutomatable = KEY_PREFIX + "label.automatable";
	private static final String labelAutomationPriority = KEY_PREFIX + "label.automationPriority";
	private static final String labelRequestStatus = KEY_PREFIX + "label.requestStatus";


	private final String milestoneName;

	public Data() {
		this(null);
	}

	public Data(String milestoneLabel) {
		super();
		this.milestoneName = milestoneLabel;
	}

	private String translate(String string) {
		return I18nHelper.translate(string);
	}

	private String translate(String string, Object[] params) {
		return I18nHelper.translate(string, params);
	}

	public String getTestcasesIndex() {
		return translate(testcasesIndex);
	}

	public String getPrerequisite() {
		return translate(prerequisite);
	}

	public String getGeneratedDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(translate(generatedDate));
		Calendar cal = Calendar.getInstance();
		return sdfDate.format(cal.getTime());
	}

	public String getGeneratedHour() {
		SimpleDateFormat sdfHour = new SimpleDateFormat(translate(generatedHour));
		Calendar cal = Calendar.getInstance();
		return sdfHour.format(cal.getTime());
	}

	public String getGeneratedTitle() {

		Object[] params = new Object[2];
		params[0] = getGeneratedDate();
		params[1] = getGeneratedHour();
		return translate(generatedTitle, params);
	}

	public String getGenerated() {
		return translate(generated);
	}

	public String getLabelDescription() {

		return translate(labelDescription);
	}

	public String getLabelAttachment() {
		return translate(labelAttachment);
	}

	public String getLabelAutomated() {
		return translate(labelAutomated);
	}

	public String getLabelCreated() {
		return translate(labelCreated);
	}

	public String getLabelId() {
		return translate(labelId);
	}

	public String getLabelImportance() {
		return translate(labelImportance);
	}

	public String getLabelLastModified() {
		return translate(labelLastModified);
	}

	public String getLabelNature() {
		return translate(labelNature);
	}

	public String getLabelPage() {
		return translate(labelPage);
	}

	public String getLabelProjet() {
		return translate(labelProjet);
	}

	public String getLabelStatus() {
		return translate(labelStatus);
	}

	public String getLabelTestcasesreport() {
		return translate(labelTestcasesreport);
	}

	public String getLabelType() {
		return translate(labelType);
	}

	public String getLabelVersion() {
		return translate(labelVersion);
	}

	public String getLinkedRequirementsName() {
		return translate(linkedRequirementsName);
	}

	public String getLinkedRequirementsProjectName() {
		return translate(linkedRequirementsProjectName);
	}

	public String getLinkedRequirementsTitle() {
		return translate(linkedRequirementsTitle);
	}

	public String getSummary() {
		return translate(summary);
	}

	public String getTestcaseCreatedBy() {
		return translate(testcaseCreatedBy);
	}

	public String getTestcaseCreatedOn() {
		return translate(testcaseCreatedOn);
	}

	public String getTestcaseExecutionMode() {
		return translate(testcaseExecutionMode);
	}

	public String getTestcaseImportance() {
		return translate(testcaseImportance);
	}

	public String getTestcaseModifiedBy() {
		return translate(testcaseModifiedBy);
	}

	public String getTestcaseModifiedOn() {
		return translate(testcaseModifiedOn);
	}

	public String getTestcaseStepsAction() {
		return translate(testcaseStepsAction);
	}

	public String getTestcaseStepsCall() {
		return translate(testcaseStepsCall);
	}

	public String getTestcaseStepsCallDataset() {
		return translate(testcaseStepsCallDataset);
	}

	public String getTestcaseStepsExpectedResult() {
		return translate(testcaseStepsExpectedResult);
	}

	public String getTestcaseStepsTitle() {
		return translate(testcaseStepsTitle);
	}

	public String getTestcasesReportLabel() {
		return translate(testcasesReportLabel);
	}

	// issue 4712
	public String getTestcasesTitle() {
		String title = translate(testcasesTitle);
		SimpleDateFormat formatter = new SimpleDateFormat(translate(testcasesTitleDateformat));
		String strDate = formatter.format(new Date());
		return title + "-" + strDate;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public String getLabelMilestones() {
		return translate(labelMilestones);
	}

	public String getTitleMilestone() {
		return (milestoneName != null) ? translate(titleMilestone) + " " + milestoneName : "";
	}

	public String getScriptTitle() {
		return translate(scriptTitle);
	}

	public String getParameterTitle() {
		return translate(parameterTitle);
	}

	public String getTableName() {
		return translate(tableName);
	}

	public String getTableDescription() {
		return translate(tableDescription);
	}

	public String getTableTestCaseSource() {
		return translate(tableTestCaseSource);
	}

	public String getTableValue() {
		return translate(tableValue);
	}

	public String getDatasetTitle() {
		return translate(datasetTitle);
	}

	public String getLabelAutomatable() {
		return translate(labelAutomatable);
	}

	public String getLabelAutomationPriority() {
		return translate(labelAutomationPriority);
	}

	public String getLabelRequestStatus() {
		return translate(labelRequestStatus);
	}

}
