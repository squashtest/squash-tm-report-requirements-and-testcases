/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.util.ArrayList;
import java.util.List;

public class TestCaseSteps {

	private Long id;

	private String action;

	private String expectedResult;

	private String type;

	private Long order;

	private String dataset;

	private String prerequisites;

	private Long attach;

	private Long requirement;

	private List<LinkedRequirementsProject> linkedRequirementsProjects = new ArrayList<>();

	private Long testCaseId;

	private boolean printAttachments;

	private boolean printCufs;

	private boolean printStepLinkedReq;

	public String getAttach() {
		if (attach == 0){
			return I18nHelper.translate(Data.KEY_PREFIX+"steps.attachment.none");
		} else {
			return String.valueOf(attach);
		}
	}

	public void setAttach(Long attach) {
		this.attach = attach;
	}

	public String getRequirement() {
		if (requirement == 0){
			return I18nHelper.translate(Data.KEY_PREFIX+"steps.requirement.none");
		} else {
			return String.valueOf(requirement);
		}
	}

	public void setRequirement(Long requirement) {
		this.requirement = requirement;
	}

	private List<Cuf> rtfCufs;
	private List<Cuf> cufs;
	private List<Cuf> tagCufs;
	private List<Cuf> numCufs;


	public List<Cuf> getTagCufs() {
		return tagCufs;
	}

	public void setTagCufs(List<Cuf> tagcufs) {
		this.tagCufs = tagcufs;
	}

	public List<Cuf> getNumCufs() {
		return numCufs;
	}

	public void setNumCufs(List<Cuf> numCufs) {
		this.numCufs = numCufs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setOrder(Long order) {
		this.order = order;
	}

	public Long getOrder() {
		return order + 1;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getPrerequisites() {
		return prerequisites;
	}

	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}

	public void setCufs(List<Cuf> cufs) {
		this.cufs = cufs;
	}

	public void setRtfCufs(List<Cuf> cufs) {
		this.rtfCufs = cufs;
	}

	public List<Cuf> getCufs() {
		return cufs;
	}

	public List<Cuf> getRtfCufs() {
		List<Cuf> list = new ArrayList<Cuf>(rtfCufs);
		return list;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public boolean acceptAsCuf(Cuf cufBean) {
		return  cufBean.getEntityId().equals(id);
	}

	public List<LinkedRequirementsProject> getLinkedRequirementsProjects() {
		return linkedRequirementsProjects;
	}

	public void setLinkedRequirementsProjects(List<LinkedRequirementsProject> linkedRequirementsProjects) {
		this.linkedRequirementsProjects = linkedRequirementsProjects;
	}

	public boolean isPrintAttachments() {
		return printAttachments;
	}

	public void setPrintAttachments(boolean printAttachments) {
		this.printAttachments = printAttachments;
	}

	public boolean isPrintCufs() {
		return printCufs;
	}

	public void setPrintCufs(boolean printCufs) {
		this.printCufs = printCufs;
	}

	public boolean isPrintStepLinkedReq() {
		return printStepLinkedReq;
	}

	public void setPrintStepLinkedReq(boolean printStepLinkedReq) {
		this.printStepLinkedReq = printStepLinkedReq;
	}
}
