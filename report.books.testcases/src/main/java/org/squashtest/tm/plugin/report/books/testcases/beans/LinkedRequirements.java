/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

public class LinkedRequirements {

	private Long id;

	private String name;

	private String reference ;

	private String projectName;

	private String criticality;

	private Long version;

	private Long testCaseId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getCriticality() {

		String message = "";
		switch (criticality) {
			case "MAJOR":
				message = "report.books.testcases.linked.requirement.criticality.major";
				break;
			case "MINOR":
				message = "report.books.testcases.linked.requirement.criticality.minor";
				break;
			case "CRITICAL":
				message = "report.books.testcases.linked.requirement.criticality.critical";
				break;
			default:
				message = "report.books.testcases.linked.requirement.criticality.undefined";
				break;
		}
		return I18nHelper.translate(message);
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}
}
