package org.squashtest.tm.plugin.report.books.testcases.query;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.plugin.report.books.testcases.beans.Cuf;
import org.squashtest.tm.plugin.report.books.testcases.beans.Dataset;
import org.squashtest.tm.plugin.report.books.testcases.beans.DatasetParamValue;
import org.squashtest.tm.plugin.report.books.testcases.beans.I18nHelper;
import org.squashtest.tm.plugin.report.books.testcases.beans.LinkedRequirements;
import org.squashtest.tm.plugin.report.books.testcases.beans.LinkedRequirementsProject;
import org.squashtest.tm.plugin.report.books.testcases.beans.Node;
import org.squashtest.tm.plugin.report.books.testcases.beans.Parameter;
import org.squashtest.tm.plugin.report.books.testcases.beans.TestCase;
import org.squashtest.tm.plugin.report.books.testcases.beans.TestCaseSteps;
import org.squashtest.tm.plugin.report.books.testcases.foundation.CufType;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class TestCasesTreeFormatter {
	private static final String END_SEPARATOR_PLACEHOLDER = "=Sep=";
	private static final String SORTING_CHAIN_SEPARATOR = " ";
	private static final String CHAIN_SEPARATOR = " > ";
	private static final String SEPARATOR_PLACEHOLDER = END_SEPARATOR_PLACEHOLDER + ",";
	private static final String TAG_EMPTY = "";
	private static final String TAG_SEPARATOR = "|";

	private List<Integer> paragraphs;

	private List<Long> tcIdsAlreadyAdd;

	public Collection<TestCaseSteps> toTestCaseSteps(Collection<Object[]> testCaseStepsData, boolean printAttachments,
													 boolean printLinkedReq, boolean printCufs) {

		Collection<TestCaseSteps> result = new ArrayList<>();
		for (Object[] array: testCaseStepsData) {
			TestCaseSteps testCaseStepsBean = new TestCaseSteps();
			testCaseStepsBean.setId(evaluateExpressionToLong(array[0]));
			String action = evaluateExpressionToString(array[1]);
			if("C".equals(evaluateExpressionToString(array[3]))) {
				List<String> params = new ArrayList<>();
				params.add(action);
				action = I18nHelper.translate("report.books.testcases.steps.call", params.toArray());
				if(!"".equals(evaluateExpressionToString(array[5]))) {
					params.add(evaluateExpressionToString(array[5]));
					action = I18nHelper.translate("report.books.testcases.steps.call.dataset", params.toArray());
				}
			}
			testCaseStepsBean.setAction(action);
			testCaseStepsBean.setExpectedResult(evaluateExpressionToString(array[2]));
			testCaseStepsBean.setType(evaluateExpressionToString(array[3]));
			testCaseStepsBean.setOrder(evaluateExpressionToLong(array[4]));
			testCaseStepsBean.setDataset(evaluateExpressionToString(array[5]));
			testCaseStepsBean.setAttach(evaluateExpressionToLong(array[6]));
			testCaseStepsBean.setRequirement(evaluateExpressionToLong(array[7]));
			testCaseStepsBean.setTestCaseId(evaluateExpressionToLong(array[8]));
			testCaseStepsBean.setPrintAttachments(printAttachments);
			testCaseStepsBean.setPrintStepLinkedReq(printLinkedReq);
			testCaseStepsBean.setPrintCufs(printCufs);
			result.add(testCaseStepsBean);
		}
		return result;
	}

	public Collection<Cuf> toCufBean(Collection<Object[]> cufsData) {

		Collection<Cuf> result = new ArrayList<>();
		for (Object[] array: cufsData) {
			Cuf cufBean = new Cuf();
			cufBean.setValue(evaluateExpressionToString(array[0]));
			cufBean.setLabel(evaluateExpressionToString(array[1]));
			cufBean.setType(evaluateExpressionToString(array[2]));
			cufBean.setEntityId(evaluateExpressionToLong(array[3]));
			result.add(cufBean);
		}
		return result;
	}

	Collection<Cuf> toTagCufBean(Collection<Object[]> tagCufData) {
		Collection<Cuf> cufs = new ArrayList<>();
		for (Object[] row : tagCufData) {
			Cuf cuf = new Cuf();
			if (row[0] != null) {
				String chain = evaluateExpressionToString(row[0]).replaceAll(SEPARATOR_PLACEHOLDER, TAG_SEPARATOR);
				cuf.setValue(evaluateExpressionToString(chain.substring(0, chain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			} else {
				cuf.setValue(TAG_EMPTY);
			}
			cuf.setLabel(evaluateExpressionToString(row[1]));
			cuf.setType(evaluateExpressionToString(row[2]));
			cuf.setEntityId(evaluateExpressionToLong(row[3]));
			cufs.add(cuf);
		}
		return cufs;
	}

	public Collection<LinkedRequirements> toLinkedRequirementBean(Collection<Object[]> linkedRequirementData) {

		Collection<LinkedRequirements> result = new ArrayList<>();
		for(Object[] array: linkedRequirementData) {
			LinkedRequirements linkedRequirementsBean = new LinkedRequirements();
			linkedRequirementsBean.setId(evaluateExpressionToLong(array[0]));
			linkedRequirementsBean.setName(evaluateExpressionToString(array[1]));
			linkedRequirementsBean.setProjectName(evaluateExpressionToString(array[2]));
			linkedRequirementsBean.setCriticality(evaluateExpressionToString(array[3]));
			linkedRequirementsBean.setVersion(evaluateExpressionToLong(array[4]));
			linkedRequirementsBean.setReference(evaluateExpressionToString(array[5]));
			linkedRequirementsBean.setTestCaseId(evaluateExpressionToLong(array[6]));
			result.add(linkedRequirementsBean);
		}
		return result;
	}

	public Collection<TestCase> toTestCaseBean(Collection<Object[]> testCaseData, boolean printLinkedReq, boolean printParameters) {

		Collection<TestCase> result = new ArrayList<>();
		for(Object[] array: testCaseData) {
			TestCase testcase = new TestCase();
			testcase.setFolder(evaluateExpressionToLong(array[2]));

			testcase.setId(evaluateExpressionToLong(array[1]));

			testcase.setName(evaluateExpressionToString(array[3]));
			testcase.setLevel(evaluateExpressionToLong(array[4]));
			testcase.setImportance(evaluateExpressionToString(array[5]));
			testcase.setNature(evaluateExpressionToString(array[6]));
			testcase.setType(evaluateExpressionToString(array[7]));
			testcase.setNatureType(evaluateExpressionToString(array[8]));
			testcase.setTypeType(evaluateExpressionToString(array[9]));
			testcase.setStatus(evaluateExpressionToString(array[10]));
			testcase.setExecutionMode(evaluateExpressionToLong(array[11]));
			testcase.setPrerequisites(evaluateExpressionToString(array[12]));
			testcase.setReference(evaluateExpressionToString(array[13]));
			testcase.setCreatedOn(evaluateExpressionToString(array[14]));
			testcase.setCreatedBy(evaluateExpressionToString(array[15]));
			testcase.setLastModifiedOn(evaluateExpressionToString(array[16]));
			testcase.setLastModifiedBy(evaluateExpressionToString(array[17]));
			testcase.setDescription(evaluateExpressionToString(array[18]));
			testcase.setAttachments(evaluateExpressionToLong(array[19]));
			String sortingChain = evaluateExpressionToString(array[20]).replaceAll(SEPARATOR_PLACEHOLDER,
				SORTING_CHAIN_SEPARATOR);
			testcase.setSortingChain(HtmlUtils.htmlEscape(sortingChain.substring(0,
				sortingChain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			String milestoneLabels = evaluateExpressionToString(array[21]);
			testcase.setMilestoneNames(milestoneLabels);
			String nodeIds = evaluateExpressionToString(array[22]);
			String[] nodesIds = nodeIds.split(SEPARATOR_PLACEHOLDER);
			List<Long> hierachyNodes = new ArrayList<>();
			for(String node: nodesIds) {
				hierachyNodes.add(Long.parseLong(node.replace(END_SEPARATOR_PLACEHOLDER, "")));
			}
			testcase.setNodeIds(hierachyNodes);
			if (testcase.getFolder() == 1L) {
				String chain = evaluateExpressionToString(array[0]).replaceAll(SEPARATOR_PLACEHOLDER, CHAIN_SEPARATOR);
				testcase.setChain(HtmlUtils.htmlEscape(chain.substring(0, chain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			} else {
				String chain = evaluateExpressionToString(array[0]);
				int index = chain.lastIndexOf(SEPARATOR_PLACEHOLDER);
				if(index >= 0) {
					chain = chain.substring(0, index);
				}
				chain = chain.replace(SEPARATOR_PLACEHOLDER, CHAIN_SEPARATOR);
				testcase.setChain(chain);
			}

			testcase.setScript(evaluateExpressionToString(array[23]));
			testcase.setKind(evaluateExpressionToString(array[24]));
			testcase.setAllowAutomationWorkflow(evaluateExpressionToLong(array[25]));
			testcase.setAutomatable(evaluateExpressionToString(array[26]));
			testcase.setRequestStatus(evaluateExpressionToString(array[27]));
			testcase.setAutomationPriority(evaluateExpressionToString(array[28]));
			testcase.setPrintLinkedReq(printLinkedReq);
			testcase.setPrintParameters(printParameters);
			result.add(testcase);

		}
		return result;
	}

	List<Node> toNodesBean(Collection<Object[]> nodesData) {
		List<Node> nodeBeans = new ArrayList<>();
		for(Object[] array: nodesData) {
			Node nodeBean = new Node();
			nodeBean.setProjectId(evaluateExpressionToLong(array[0]));
			nodeBean.setProjectName(evaluateExpressionToString(array[1]));
			nodeBean.setItemId(evaluateExpressionToLong(array[2]));
			nodeBeans.add(nodeBean);
		}
		return nodeBeans;
	}

	Collection<TestCaseSteps> toCallStepBean(Collection<Object[]> objects, Long idTestCase, boolean printAttachments,
											 boolean printLinkedReq, boolean printCufs) {
		Collection<TestCaseSteps> result = new ArrayList<>();
		for (Object[] array: objects) {
			TestCaseSteps testCaseStepsBean = new TestCaseSteps();
			testCaseStepsBean.setId(evaluateExpressionToLong(array[0]));
			testCaseStepsBean.setAction(evaluateExpressionToString(array[1]));
			testCaseStepsBean.setExpectedResult(evaluateExpressionToString(array[2]));
			testCaseStepsBean.setType(evaluateExpressionToString(array[3]));
			testCaseStepsBean.setOrder(evaluateExpressionToLong(array[4]));
			testCaseStepsBean.setDataset(evaluateExpressionToString(array[5]));
			testCaseStepsBean.setAttach(evaluateExpressionToLong(array[6]));
			testCaseStepsBean.setRequirement(evaluateExpressionToLong(array[7]));
			testCaseStepsBean.setTestCaseId(evaluateExpressionToLong(idTestCase));
			testCaseStepsBean.setPrintAttachments(printAttachments);
			testCaseStepsBean.setPrintCufs(printCufs);
			testCaseStepsBean.setPrintStepLinkedReq(printLinkedReq);
			result.add(testCaseStepsBean);
		}
		return result;
	}

	Collection<Parameter> toParameter(Collection<Object[]> objects) {
		Collection<Parameter> parameters = new ArrayList<>();
		for(Object[] array: objects) {
			Parameter parameter = new Parameter();
			parameter.setName(evaluateExpressionToString(array[0]));
			parameter.setDescription(evaluateExpressionToString(array[1]));
			parameter.setTestCaseId(evaluateExpressionToLong(array[2]));
			parameter.setTestCaseSourceName(evaluateExpressionToString(array[3]));
			parameter.setTestCaseSourceRef(evaluateExpressionToString(array[4]));
			parameter.setTestCaseSourceProjectName(evaluateExpressionToString(array[5]));
			parameters.add(parameter);
		}
		return parameters;
	}

	Collection<Dataset> toDataSet(Collection<Object[]> objects) {
		Collection<Dataset> datasets = new ArrayList<>();
		for(Object[] array: objects) {
			Dataset dataset = new Dataset();
			dataset.setId(evaluateExpressionToLong(array[0]));
			dataset.setName(evaluateExpressionToString(array[1]));
			dataset.setTestCaseId(evaluateExpressionToLong(array[2]));
			datasets.add(dataset);
		}
		return datasets;
	}

	Collection<DatasetParamValue> toDataSetParamValue(Collection<Object[]> objects) {
		Collection<DatasetParamValue> datasetParamValues = new ArrayList<>();
		for(Object[] array: objects) {
			DatasetParamValue datasetParamValue = new DatasetParamValue();
			datasetParamValue.setDataSetId(evaluateExpressionToLong(array[0]));
			datasetParamValue.setName(evaluateExpressionToString(array[1]));
			datasetParamValue.setValue(evaluateExpressionToString(array[2]));
			datasetParamValues.add(datasetParamValue);
		}
		return datasetParamValues;
	}

	public void bindAll(Collection<TestCaseSteps> testCaseStepsBeans, Map<String, Collection<Cuf>> testCaseCufMap,
						Map<String, Collection<Cuf>> stepCufMap, Collection<LinkedRequirements> linkedRequirementsBeans,
						Collection<TestCase> testCaseBeans, Collection<LinkedRequirements> linkedRequirementsBySteps,
						Collection<Parameter> parameters, Collection<Dataset> datasets, Collection<DatasetParamValue> datasetParamValues, Collection<Node> nodes) {

		tcIdsAlreadyAdd = new ArrayList<>();
		paragraphs = new ArrayList<>();
		if(!testCaseStepsBeans.isEmpty()) {
			bindCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.CUFS.getValue()));
			bindNumCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.NUM_CUFS.getValue()));
			bindTagCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.TAG_CUFS.getValue()));
			bindRtfCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.RTF_CUFS.getValue()));
			bindLinkedReqToTestStep(testCaseStepsBeans, linkedRequirementsBySteps);
			bindTestStepToTestCase(testCaseBeans, testCaseStepsBeans);
			updateStepOrder(testCaseBeans);
		}
		bindParamValuesToDataset(datasets, datasetParamValues);
		bindDatasetToTestCase(datasets, testCaseBeans);
		bindCufToTestCase(testCaseBeans, testCaseCufMap.get(CufType.CUFS.getValue()));
		bindNumCufToTestCase(testCaseBeans, testCaseCufMap.get(CufType.NUM_CUFS.getValue()));
		bindTagCufTestCase(testCaseBeans, testCaseCufMap.get(CufType.TAG_CUFS.getValue()));
		bindRtfCufToTestCase(testCaseBeans, testCaseCufMap.get(CufType.RTF_CUFS.getValue()));
		bindParameterForTestCase(testCaseBeans, parameters);
		if(!linkedRequirementsBeans.isEmpty()) {
			bindLinkedReqToTc(testCaseBeans, linkedRequirementsBeans);
		}

		bindTestCaseToNode(testCaseBeans, nodes);
	}

	private void updateStepOrder(Collection<TestCase> testCases) {

		for (TestCase testCase: testCases) {
			Long order = 0L;
			for (TestCaseSteps testCaseSteps: testCase.getTcSteps()) {
				testCaseSteps.setOrder(order);
				order++;
			}
		}
	}

	private void bindTestCaseToNode(Collection<TestCase> testCaseBeans, Collection<Node> nodeBeans) {
		for(Node nodeBean: nodeBeans) {
			List<TestCase> testCaseBeans1 = groupTestCaseForNode(testCaseBeans,nodeBean);
			nodeBean.setTestCasesBeans(testCaseBeans1);
		}
	}

	private void bindTestStepToTestCase(Collection<TestCase> testCaseBeans, Collection<TestCaseSteps> testCaseStepsBeans) {
		for(TestCase testCaseBean: testCaseBeans) {
			List<TestCaseSteps> testCaseStepBeanList = groupTestStepForTestCase(testCaseStepsBeans, testCaseBean);
			testCaseBean.setTcSteps(testCaseStepBeanList);
		}
	}

	private void bindLinkedReqToTc(Collection<TestCase> testCaseBeans, Collection<LinkedRequirements>linkedRequirementsBeans) {
		for(TestCase testCaseBean: testCaseBeans) {
			List<LinkedRequirements> linkedRequirementsBeanList = groupLinkedRequirementForTestCase(linkedRequirementsBeans, testCaseBean);
			testCaseBean.setLinkedRequirementsProject(processLinkedRequirementByProject(linkedRequirementsBeanList));
		}
	}

	private void bindLinkedReqToTestStep(Collection<TestCaseSteps> testCaseSteps, Collection<LinkedRequirements>linkedRequirementsBeans) {
		for(TestCaseSteps caseSteps: testCaseSteps) {
			List<LinkedRequirements> linkedRequirementsBeanList = groupLinkedRequirementForTestStep(linkedRequirementsBeans, caseSteps);
			caseSteps.setLinkedRequirementsProjects(processLinkedRequirementByProject(linkedRequirementsBeanList));
		}
	}

	private static <K> List<LinkedRequirementsProject> processLinkedRequirementByProject(
		List<LinkedRequirements> linkedRequirements) {

		Map<String, LinkedRequirementsProject> map = new HashMap<>();
		for (LinkedRequirements req : linkedRequirements) {
			LinkedRequirementsProject proj = map.get(req.getProjectName());
			if (proj != null) {
				proj.addLinkedRequirement(req);
			} else {
				proj = new LinkedRequirementsProject();
				proj.setProjectName(req.getProjectName());
				proj.addLinkedRequirement(req);
				map.put(proj.getProjectName(), proj);
			}

		}
		return new ArrayList<>(map.values());
	}


	private void bindCufToStep(Collection<TestCaseSteps> testCaseStepsBeans,
							   Collection<Cuf> cufBeans) {
		for (TestCaseSteps testCaseStepsBean: testCaseStepsBeans) {
			List<Cuf> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setCufs(cufs);
		}

	}

	private void bindNumCufToStep(Collection<TestCaseSteps> testCaseStepsBeans,
								  Collection<Cuf> cufBeans) {
		for (TestCaseSteps testCaseStepsBean: testCaseStepsBeans) {
			List<Cuf> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setNumCufs(cufs);
		}

	}

	private void bindTagCufToStep(Collection<TestCaseSteps> testCaseStepsBeans,
								  Collection<Cuf> cufBeans) {
		for (TestCaseSteps testCaseStepsBean: testCaseStepsBeans) {
			List<Cuf> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setTagCufs(cufs);
		}

	}

	private void bindRtfCufToStep(Collection<TestCaseSteps> testCaseStepsBeans,
								  Collection<Cuf> cufBeans) {
		for (TestCaseSteps testCaseStepsBean: testCaseStepsBeans) {
			List<Cuf> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setRtfCufs(cufs);
		}

	}

	private void bindCufToTestCase(Collection<TestCase> testCaseBeans,
								   Collection<Cuf> cufBeans) {
		for (TestCase testCaseBean: testCaseBeans) {
			List<Cuf> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setCufs(cufs);
		}

	}

	private void bindNumCufToTestCase(Collection<TestCase> testCaseBeans,
									  Collection<Cuf> cufBeans) {
		for (TestCase testCaseBean: testCaseBeans) {
			List<Cuf> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setNumCufs(cufs);
		}

	}

	private void bindParameterForTestCase(Collection<TestCase> testCases, Collection<Parameter> parameters) {
		for (TestCase testCase: testCases) {
			List<Parameter> params = groupParameterForTestCase(parameters, testCase);
			testCase.setParameters(params);
		}
	}

	private void bindParamValuesToDataset(Collection<Dataset> datasets, Collection<DatasetParamValue> datasetParamValues) {
		for(Dataset dataset: datasets) {
			List<DatasetParamValue> paramValues = groupParamValueToDataset(datasetParamValues, dataset);
			dataset.setDatasetParamValues(paramValues);
		}
	}

	private void bindDatasetToTestCase(Collection<Dataset> datasets, Collection<TestCase> testCases) {
		for(TestCase testCase: testCases) {
			List<Dataset> datasetList = groupDatasetToTestCase(datasets, testCase);
			testCase.setDatasets(datasetList);
		}
	}

	private void bindTagCufTestCase(Collection<TestCase> testCaseBeans,
									Collection<Cuf> cufBeans) {
		for (TestCase testCaseBean: testCaseBeans) {
			List<Cuf> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setTagCufs(cufs);
		}

	}

	private void bindRtfCufToTestCase(Collection<TestCase> testCaseBeans,
									  Collection<Cuf> cufBeans) {
		for (TestCase testCaseBean: testCaseBeans) {
			List<Cuf> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setRtfCufs(cufs);
		}

	}

	public List<Cuf> groupCufsForTestCases(Collection<Cuf> cufData, TestCase testCaseBean) {
		List<Cuf> cufs = new ArrayList<>();
		for(Cuf cuf: cufData) {
			if (testCaseBean.acceptAsCuf(cuf)) {
				Cuf newCuf = new Cuf();
				newCuf.setLabel(cuf.getLabel());
				newCuf.setEntityId(cuf.getEntityId());
				newCuf.setType(cuf.getType());
				newCuf.setValue(cuf.getValue());
				cufs.add(newCuf);
			}
		}
		return cufs;
	}

	public List<TestCase> groupTestCaseForNode(Collection<TestCase> testCaseBeans, Node nodeBean) {
		Iterator<TestCase> iterator = testCaseBeans.iterator();
		List<TestCase> tc = new ArrayList<>();
		Set<Long> tcIds = new HashSet<>();
		while (iterator.hasNext()) {

			TestCase next = iterator.next();
			if(next.getNodeIds().contains(nodeBean.getItemId())) {
				tcIds.addAll(next.getNodeIds());
			}
		}

		Iterator<TestCase> iterator1 = testCaseBeans.iterator();
		while(iterator1.hasNext()) {
			TestCase testCaseBean = iterator1.next();
			if(tcIds.contains(testCaseBean.getId()) && !tcIdsAlreadyAdd.contains(testCaseBean.getId())) {
				changeParagraph(testCaseBean);
				testCaseBean.setParagraph(paragraphs);
				tc.add(testCaseBean);
				tcIdsAlreadyAdd.add(testCaseBean.getId());
			}
		}
		return tc;
	}

	private void changeParagraph(TestCase testCaseBean) {

		if(testCaseBean.isFolder()) {
			if (paragraphs.isEmpty()) {
				paragraphs.add(1);
			} else {
				paragraphs.set(0, paragraphs.get(0) + 1);
				paragraphs.subList(1, paragraphs.size()).clear();
			}
		} else {
			if(paragraphs.isEmpty()) {
				List<Integer> firstParagraph = Arrays.asList(1,1);
				paragraphs.addAll(firstParagraph);
			} else if (paragraphs.size() == 2) {
				paragraphs.set(1, paragraphs.get(1) + 1);
			} else {
				paragraphs.add(1);
			}
		}
	}

	public List<TestCaseSteps> groupTestStepForTestCase(Collection<TestCaseSteps> testCaseStepsBeans, TestCase testCaseBean) {
		Iterator<TestCaseSteps> iterator = testCaseStepsBeans.iterator();
		List<TestCaseSteps> tc = new ArrayList<>();
		while (iterator.hasNext()) {

			TestCaseSteps next = iterator.next();
			if(testCaseBean.getId().equals(next.getTestCaseId())) {
				tc.add(next);
				iterator.remove();
			}
		}
		return tc;
	}

	public List<Parameter> groupParameterForTestCase(Collection<Parameter> parameters, TestCase testCase) {
		List<Parameter> params = new ArrayList<>();
		for(Parameter param: parameters) {
			if(testCase.getId().equals(param.getTestCaseId())) {
				params.add(param);
			}
		}
		return params;
	}

	public List<LinkedRequirements> groupLinkedRequirementForTestCase(Collection<LinkedRequirements> linkedRequirementsBeans, TestCase testCaseBean) {
		Iterator<LinkedRequirements> iterator = linkedRequirementsBeans.iterator();
		List<LinkedRequirements> linkedRequirementsBeanArrayList = new ArrayList<>();
		while (iterator.hasNext()) {

			LinkedRequirements next = iterator.next();
			if(testCaseBean.getId().equals(next.getTestCaseId())) {
				linkedRequirementsBeanArrayList.add(next);
				iterator.remove();
			}
		}
		return linkedRequirementsBeanArrayList;
	}

	public List<LinkedRequirements> groupLinkedRequirementForTestStep(Collection<LinkedRequirements> linkedRequirementsBeans, TestCaseSteps testCaseSteps) {
		Iterator<LinkedRequirements> iterator = linkedRequirementsBeans.iterator();
		List<LinkedRequirements> linkedRequirementsBeanArrayList = new ArrayList<>();
		for(LinkedRequirements linkedRequirements: linkedRequirementsBeans) {
			if(testCaseSteps.getId().equals(linkedRequirements.getTestCaseId())) {
				linkedRequirementsBeanArrayList.add(linkedRequirements);
			}
		}
		return linkedRequirementsBeanArrayList;
	}

	public List<Cuf> groupCufsForTestSteps(Collection<Cuf> cufData, TestCaseSteps testCaseStepsBean) {
		List<Cuf> cufs = new ArrayList<>();
		for(Cuf cuf : cufData) {
			if (testCaseStepsBean.acceptAsCuf(cuf)) {
				Cuf newCuf = new Cuf();
				newCuf.setLabel(cuf.getLabel());
				newCuf.setEntityId(cuf.getEntityId());
				newCuf.setType(cuf.getType());
				newCuf.setValue(cuf.getValue());
				cufs.add(newCuf);
			}
		}
		return cufs;
	}

	private List<DatasetParamValue> groupParamValueToDataset(Collection<DatasetParamValue> datasetParamValues, Dataset dataset) {
		List<DatasetParamValue> result = new ArrayList<>();
		for(DatasetParamValue datasetParamValue : datasetParamValues) {
			if(dataset.getId().equals(datasetParamValue.getDataSetId())) {
				result.add(datasetParamValue);
			}
		}
		return result;
	}

	private List<Dataset> groupDatasetToTestCase(Collection<Dataset> datasets, TestCase testCase) {
		List<Dataset> result = new ArrayList<>();
		for(Dataset dataset : datasets) {
			if(dataset.getTestCaseId().equals(testCase.getId())) {
				result.add(dataset);
			}
		}
		return result;
	}

	private Locale currentLocale() {
		Locale current = LocaleContextHolder.getLocale();

		if (current == null) {
			current = Locale.getDefault();
		}

		return current;
	}

	private String convertDatetoString(Date date) {

		return DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
			DateFormat.SHORT, currentLocale()).format(date);

	}
	private String evaluateExpressionToString(Object obj) {

		if (obj instanceof Date){
			return obj != null ? convertDatetoString((Date) obj): "";
		}

		return obj != null ? obj.toString() : "";

	}

	protected Long evaluateExpressionToLong(Object obj) {

		return Long.valueOf(obj.toString());

	}
}
