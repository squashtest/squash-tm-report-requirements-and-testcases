/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component("report.books.testcases.i18nHelper")
public class I18nHelper implements MessageSourceAware {

	private static MessageSource msgSource;

	private static Locale currentLocale() {
		Locale current = LocaleContextHolder.getLocale();
		if (current == null) {
			current = Locale.getDefault();
		}
		return current;
	}

	public static String translate(String string) {
		return msgSource.getMessage(string, null, currentLocale());
	}

	public static String translate(String string, Object[] params){
		return msgSource.getMessage(string, params, currentLocale());
	}


	@Override
	public void setMessageSource(MessageSource messageSource) {
		msgSource = messageSource;

	}

}
