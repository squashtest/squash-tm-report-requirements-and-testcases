package org.squashtest.tm.plugin.report.books.testcases.query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.squashtest.tm.api.repository.SqlQueryRunner;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TestCasesTreeQueryFinder {

	private static final String TAGS = "tags";
	private static final String MILESTONES = "milestones";
	private static final String ENTITY_TYPE = "entityType";
	private static final String ENTITY_IDS = "entityIds";
	private static final String PROJECT_IDS = "projectIds";
	private static final String NODE_IDS = "nodeIds";
	private static final String TEST_CASE_IDS = "testcaseIds";
	private static final String TEST_CASE_ID = "testcaseId";

	private String idsByProjectQuery;
	private String idsForAllProjectsQuery;
	private String idsBySelectionQuery;
	private String idsByMilestoneQuery;
	private String idsByTagQuery;
	private String testCasesQuery;
	private String linkedReqQuery;
	private String testCaseStepsQuery;
	private String cufQuery;
	private String rtfCufQuery;
	private String tagCufQuery;
	private String numCufQuery;
	private String nodesByTestCaseIdsQuery;
	private String nodesByMilestonesQuery;
	private String nodesByTagsQuery;
	private String nodesByProjectIdsQuery;
	private String testStepsByCallSteps;
	private String linkedReqByTestStepQuery;
	private String parametersQuery;
	private String dataSetQuery;
	private String dataSetParamValueQuery;

	SqlQueryRunner runner;

	void setIdsByProjectQuery(String idsByProjectQuery) {
		this.idsByProjectQuery = idsByProjectQuery;
	}

	void setIdsForAllProjectsQuery(String idsForAllProjectsQuery) {
		this.idsForAllProjectsQuery = idsForAllProjectsQuery;
	}

	void setIdsBySelectionQuery(String idsBySelectionQuery) {
		this.idsBySelectionQuery = idsBySelectionQuery;
	}

	void setIdsByMilestoneQuery(String idsByMilestoneQuery) {
		this.idsByMilestoneQuery = idsByMilestoneQuery;
	}

	void setIdsByTagQuery(String idsByTagQuery) {
		this.idsByTagQuery = idsByTagQuery;
	}

	void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	void setTestCasesQuery(String testCasesQuery) {
		this.testCasesQuery = testCasesQuery;
	}

	void setTestCaseStepsQuery(String testCaseStepsQuery) {
		this.testCaseStepsQuery = testCaseStepsQuery;
	}

	void setLinkedReqQuery(String linkedReqQuery) {
		this.linkedReqQuery = linkedReqQuery;
	}

	void setCufQuery(String cufQuery) {
		this.cufQuery = cufQuery;
	}

	void setRtfCufQuery(String rtfCufQuery) {
		this.rtfCufQuery = rtfCufQuery;
	}

	void setTagCufQuery(String tagCufQuery) {
		this.tagCufQuery = tagCufQuery;
	}

	void setNumCufQuery(String numCufQuery) {
		this.numCufQuery = numCufQuery;
	}

	void setNodesByTestCaseIdsQuery(String nodesByTestCaseIdsQuery) {
		this.nodesByTestCaseIdsQuery = nodesByTestCaseIdsQuery;
	}

	void setNodesByMilestonesQuery(String nodesByMilestonesQuery) {
		this.nodesByMilestonesQuery = nodesByMilestonesQuery;
	}

	void setNodesByTagsQuery(String nodesByTagsQuery) {
		this.nodesByTagsQuery = nodesByTagsQuery;
	}

	void setNodesByProjectIdsQuery(String nodesByProjectIdsQuery) {
		this.nodesByProjectIdsQuery = nodesByProjectIdsQuery;
	}

	void setTestStepsByCallSteps(String testStepsByCallSteps) {
		this.testStepsByCallSteps = testStepsByCallSteps;
	}

	void setLinkedReqByTestStepQuery(String linkedReqByTestStepQuery) {
		this.linkedReqByTestStepQuery = linkedReqByTestStepQuery;
	}

	void setParametersQuery(String parametersQuery) {
		this.parametersQuery = parametersQuery;
	}

	void setDataSetQuery(String dataSetQuery) {
		this.dataSetQuery = dataSetQuery;
	}

	void setDataSetParamValueQuery(String dataSetParamValueQuery) {
		this.dataSetParamValueQuery = dataSetParamValueQuery;
	}

	Collection<Long> findIdsByProject(Collection<String> projectStrIds) {

		List<BigInteger> foundStrIds;

		if (projectStrIds == null) {
			Map<String, Collection<Long>> params = new HashMap<>();
			foundStrIds = runner.executeSelect(idsForAllProjectsQuery, params);
			return toIdList(foundStrIds);
		}

		if (projectStrIds.isEmpty()) {
			return Collections.emptyList();
		}

		Collection<Long> projectIds = toIdList(projectStrIds);

		Map<String, Collection<Long>> params = new HashMap<>(1);
		params.put(PROJECT_IDS, projectIds);

		foundStrIds = runner.executeSelect(idsByProjectQuery, params);

		return toIdList(foundStrIds);
	}

	Collection<Long> findIdsBySelection(Collection<String> ids) {

		List<Long> versionIds = new LinkedList<>();

		// add the requirements within the given folders
		if (ids != null && (!ids.isEmpty())) {

			Map<String, Collection<Long>> params = new HashMap<>(1);
			params.put(NODE_IDS, toIdList(ids));

			List<BigInteger> foundStrIds = runner.executeSelect(idsBySelectionQuery, params);

			versionIds.addAll(toIdList(foundStrIds));
		} else {
			versionIds = new ArrayList<>();
		}

		return versionIds;

	}

	Collection<Long> findIdsByMilestone(Collection<String> milestoneIds) {

		if (!milestoneIds.isEmpty()) {
			Map<String, Collection<Long>> params = new HashMap<String, Collection<Long>>();
			params.put(MILESTONES, toIdList(milestoneIds));
			List<BigInteger> foundIds = runner.executeSelect(idsByMilestoneQuery, params);
			return toIdList(foundIds);
		} else {
			return Collections.emptyList();
		}

	}

	Collection<Long> findIdsByTags(Collection<String> tags) {

		if (!tags.isEmpty()) {
			Map<String, Collection<String>> params = new HashMap<String, Collection<String>>();
			params.put(TAGS, tags);
			List<BigInteger> foundIds = runner.executeSelect(idsByTagQuery, params);
			return toIdList(foundIds);
		} else {
			return Collections.emptyList();
		}
	}

	Collection<Object[]> getTestCasesData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(testCasesQuery, params);
	}

	Collection<Object[]> getLinkedReqStepsData(Collection<Long> testStepsIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put("testStepIds", testStepsIds);
		return execute(linkedReqByTestStepQuery, params);
	}

	Collection<Object[]> getLinkedReqData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(linkedReqQuery, params);
	}

	Collection<Object[]> getTestCaseStepsData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(testCaseStepsQuery, params);
	}

	Collection<Object[]> getCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, cufQuery);
	}

	Collection<Object[]> getNumCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, numCufQuery);
	}

	Collection<Object[]> getRtfCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, rtfCufQuery);
	}

	Collection<Object[]> getTagCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds,tagCufQuery);
	}

	Collection<Object[]> getNodesByTestCasesIds(Collection<String> testCaseIds) {
		Map<String, Object> params = new HashMap<>(1);
		params.put(NODE_IDS, toIdList(testCaseIds));
		return execute(nodesByTestCaseIdsQuery, params);
	}

	Collection<Object[]> getNodesByMilestones(Collection<String> milestones) {
		Map<String, Object> params = new HashMap<>(1);
		params.put(MILESTONES, toIdList(milestones));
		return execute(nodesByMilestonesQuery, params);
	}

	Collection<Object[]> getNodesTags(Collection<String> tags) {
		Map<String, Object> params = new HashMap<>(1);
		params.put(TAGS, tags);
		return execute(nodesByTagsQuery, params);
	}

	Collection<Object[]> getNodesByProjectIds(Collection<String> projectIds) {
		Map<String, Object> params = new HashMap<>(1);
		params.put(PROJECT_IDS, toIdList(projectIds));
		return execute(nodesByProjectIdsQuery, params);
	}

	Collection<Object[]> getTestCaseStepsData(Long testCaseId) {
		Map<String, Long> params = new HashMap<>();
		params.put(TEST_CASE_ID, testCaseId);
		return execute(testStepsByCallSteps, params);
	}

	Collection<Object[]> getParametersData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(parametersQuery, params);
	}

	Collection<Object[]> getDataSetData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(dataSetQuery, params);
	}

	Collection<Object[]> getDataSetParamValueData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(dataSetParamValueQuery, params);
	}

	@SuppressWarnings("unchecked")
	Collection<Long> toIdList(Collection<?> ids) {
		return CollectionUtils.collect(ids, new IdTransformer());
	}

	private Collection<Object[]> executeCufQuery(String entityType, Collection<Long> entityIds, String query) {
		Map<String, Object> params = new HashMap<>();
		params.put(ENTITY_TYPE, entityType);
		params.put(ENTITY_IDS, entityIds);
		return execute(query, params);
	}

	private Collection<Object[]> execute(String query, Map<String, ?> params) {
		return runner.executeSelect(query, params);
	}
	// dirty impl
	private static class IdTransformer implements Transformer {
		public Object transform(Object arg0) {
			Class<?> argClass = arg0.getClass();

			if (argClass.equals(String.class)) {
				return Long.valueOf((String) arg0);
			} else if (argClass.equals(BigInteger.class)) {
				return ((BigInteger) arg0).longValue();
			} else if (argClass.equals(Integer.class)) {
				return Long.valueOf((Integer) arg0);
			} else {
				throw new RuntimeException("bug : IdTransformer cannto convert items of class " + argClass.getName());
			}
		}
	}
}
