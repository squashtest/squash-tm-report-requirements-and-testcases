/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.books.testcases.beans;

import java.text.ParseException;
import java.util.Date;

public class CufPair {

	private Cuf firstCuf;

	private Cuf secondCuf;

	public Cuf getFirstCuf() {
		return firstCuf;
	}

	public Cuf getSecondCuf() {
		return secondCuf;
	}

	public void setFirstCuf(Cuf firstCuf) {

		this.firstCuf = firstCuf;
	}

	public void setSecondCuf(Cuf secondCuf) {
		this.secondCuf = secondCuf;
	}

	public String getFirstCufLabel() {
		return this.firstCuf.getLabel();
	}

	public String getFirstCufType() {
		return this.firstCuf.getType();
	}

	public String getFirstCufValue() {
		return this.firstCuf.getValue();
	}

	public Date getFirstCufValueAsDate() throws ParseException {
		return Cuf.computeValueAsDate(this.firstCuf.getType(), this.firstCuf.getValue());
	}

	public String getSecondCufLabel() {
		if (this.secondCuf != null) {
			return this.secondCuf.getLabel();
		} else {
			return null;
		}
	}

	public String getSecondCufType() {
		if (this.secondCuf != null) {
			return this.secondCuf.getType();
		} else {
			return null;
		}
	}

	public String getSecondCufValue() {
		if (this.secondCuf != null) {
			return this.secondCuf.getValue();
		} else {
			return null;
		}
	}

	public Date getSecondCufValueAsDate() throws ParseException {
		if (this.secondCuf != null) {
			return Cuf.computeValueAsDate(this.secondCuf.getType(), this.secondCuf.getValue());
		} else {
			return null;
		}
	}

}
