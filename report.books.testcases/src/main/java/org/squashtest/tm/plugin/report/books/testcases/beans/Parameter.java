package org.squashtest.tm.plugin.report.books.testcases.beans;

public class Parameter {

	private String name;
	private String description;
	private Long testCaseId;
	private String testCaseSourceName;
	private String testCaseSourceRef;
	private String testCaseSourceProjectName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getTestCaseSourceName() {
		String testCaseName = "";
		if(!testCaseSourceName.isEmpty()) {
			testCaseName = testCaseSourceRef + "-"+ testCaseSourceName+ " (" + testCaseSourceProjectName+")";
		}
		return testCaseName;
	}

	public void setTestCaseSourceName(String testCaseSourceName) {
		this.testCaseSourceName = testCaseSourceName;
	}

	public String getTestCaseSourceRef() {
		return testCaseSourceRef;
	}

	public void setTestCaseSourceRef(String testCaseSourceRef) {
		this.testCaseSourceRef = testCaseSourceRef;
	}

	public String getTestCaseSourceProjectName() {
		return testCaseSourceProjectName;
	}

	public void setTestCaseSourceProjectName(String testCaseSourceProjectName) {
		this.testCaseSourceProjectName = testCaseSourceProjectName;
	}
}
